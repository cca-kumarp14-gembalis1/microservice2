const express = require('express')
const app = express()
const cors = require('cors')
var port = process.env.PORT || 8080;
app.use(express.static('static'))
app.use(express.urlencoded({extended: false}))
app.use(cors())
app.listen(port)
console.log(" Express server is running on port:"+ port )

app.get("/", (req, res) => {
    res.send("Microservice 2 Gateway by Pavan Kumar and Sai Shrikar Gembali. Please enter your date of birth and today's date for getting your Marriage predictions. Usage: host/Age?date_of_birth=mm/dd/yyyy&today_date=mm/dd/yyyy")
})
app.get('/Age', function (req, res) {
date_of_birth=Date.parse(req.query['date_of_birth'])
today_date=Date.parse(req.query['today_date'])
if ( isNaN(date_of_birth) || isNaN(today_date)) {
    res.send(" you have entered in invalid format. please Use this format: mm/dd/yyyy or month dd, yyyy")
    return
}
//var today_date = Date(Date.now());
//a = today_date.toString
var Diffdays = parseInt((today_date - date_of_birth) / (1000*60*60*24));
var Age = parseInt((Diffdays) / (365));
//res.send(`${(Age)} is your age`)

if(Age < 18){
        res.send('You are ' + Age + ' years old, you are in teenage');
}else if(Age > 18 && Age <24){
        res.send('You are ' + Age + ' years old, you are an adult, settle first before getting married');
}else if(Age > 24 && Age <35){
        res.send('Congratulations! You are ' + Age + ' years old, this is the right age for getting married');
}else{
        res.send('Seems like You are ' + Age + ' years old, you can still try for marriage');
}

})
